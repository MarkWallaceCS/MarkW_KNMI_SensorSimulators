(1)
00:00 to 10:00 of 60 minute interval
Purpose: 10 min of unchanging steady state data.
KNMI comments:
{
 # First fill out 10 minutes of recognisable data so that we can see the MD in output.
 # Afterward, change the direction, causing a Marked Discontinuity.
}
wind speed = 6.0 m/s
wind direction = 45 degrees
Expect status code reports of "0"

(2)
10:00 to 13:00 of 60 minute interval
Purpose: Change direction to cause MD.
KNMI comments:
{
 # Gather 3 minutes of samples, to see when the MD first starts being detected.
}
wind speed = 6.0 m/s
wind direction changed to 90 degrees
Expect status code report of "c" for wind direction jump.
Expect detection of a MD event:
{
10:12   WR=UR="c"
11:23.5 discontinuity_flag=True, ndiscontinuity_events = 1 <-- Start of MD.
11:35.5 discontinuity_flag=True
11:47.5 discontinuity_flag=True
11:59.5 discontinuity_flag=True
11:11.5 discontinuity_flag=True
12:23.5 discontinuity_flag=True
12:35.5 discontinuity_flag=True
}

(3)
13:00 to 13:06 of 60 minute interval
Purpose: Cause error in both wind speed and wind direction.
KNMI comments:
{
 # Gather a sample with both sensors in error state.
}
wind speed changed to (max + 1) = 76 m/s
wind direction = 90 degrees
Send extra wind direction bytes to cause "J" error.
Expect status code report of "J" for wind direction and "D" for wind speed.

(4)
13:06 to 16:18 of 60 minute interval
Purpose: Cause no errors but cause another MD by changing direction.
KNMI comments:
{
 # With another changed direction, see when the MD first starts being detected.
}
wind speed = 6.0 m/s
wind direction = 135 degrees
Expect detection of a MD event but no "c" for wind direction jump.  <MD#2><MD#3> 90-135 degrees
{
13:12   WR=UR="c"&"J", WS="D", US="D" <-- Note that "c" has higher priority than "J"
14:35.5 discontinuity_flag=True, ndiscontinuity_events = 2 <-- Start <MD#2>.
14:47.5 discontinuity_flag=True, ndiscontinuity_events = 2
14:59.5 discontinuity_flag=False
15:11.5 discontinuity_flag=True, ndiscontinuity_events = 3 <-- Start <MD#3>.
15:23.5 discontinuity_flag=True, ndiscontinuity_events = 3
15:35.5 discontinuity_flag=True, ndiscontinuity_events = 3
15:47.5 discontinuity_flag=True, ndiscontinuity_events = 3
Seemed to get split into two sections.
}
(5)
16:18 to 16:24 of 60 minute interval
Purpose: WS in error
KNMI comments:
{
 # Gather a sample with just the anemometer in error state.
}
wind speed changed to (max + 1) = 76 m/s
wind direction = 135 degrees
Expect report of "D" status report for wind speed error.

(6)
16:24 to 19:32 of 60 minute interval
wind speed changed to 6 m/s
wind direction changed to 180 degrees
KNMI comments:
{
# With another changed direction, see when the MD first starts being detected.
}
Expect detection of a MD event.  <MD#4> 135-180 degrees
16:24   WS=US="D"
16:36   WS=US="D", WR=UR="c" WS=US="D"
17:47.5 discontinuity_flag=True, ndiscontinuity_events = 4 <-- Start of <MD#4>.
17:59.5 discontinuity_flag=True, ndiscontinuity_events = 4
18:11.5 discontinuity_flag=False
18:23.5 discontinuity_flag=False
18:35.5 discontinuity_flag=True, ndiscontinuity_events = 5 <-- Start of <MD#5>.
18:47.5 discontinuity_flag=True, ndiscontinuity_events = 5
18:59.5 discontinuity_flag=True, ndiscontinuity_events = 5


(7)
19:32 to 19:38 of 60 minute interval
Purpose: WD in error
wind speed = 6 m/s
wind direction = 180 degrees
Send extra wind direction bytes to cause "J" error.
Expect status code report of "J" for wind direction.
KNMI comments:
{
# Gather a sample with just the wind vane in error state.
}

(8)
19:38 to 22:50 of 60 minute interval
Purpose: MD with direction chage
wind speed changed to 6 m/s
wind direction changed to 225 degrees
KNMI comments:
{
 # With another changed direction, see when the MD first starts being detected.
}
Expect detection of a MD event.  <MD#6> 180-225 degrees
20:59.5 discontinuity_flag=True, ndiscontinuity_events = 6 <-- Start of <MD#6>.
21:11.5 discontinuity_flag=True, ndiscontinuity_events = 6
21:23.5 discontinuity_flag=False
21:35.5 discontinuity_flag=True, ndiscontinuity_events = 7 <-- Start of <MD#7>.
21:47.5 discontinuity_flag=True, ndiscontinuity_events = 7
21:59.5 discontinuity_flag=True, ndiscontinuity_events = 7
22:11.5 discontinuity_flag=True, ndiscontinuity_events = 7

(9)
22:50 to 22:56 of 60 minute interval
Purpose:
wind speed changed to (max + 1) = 76 m/s
wind direction = 225 degrees
Send extra wind direction bytes to cause "J" error.
Expect status code report of "J" for wind direction.
KNMI comments:
{
 # Gather a sample with multiple samples in a row having an error state.
}

(10)
22:56 to 23:56 of 60 minute interval
Purpose:
wind speed changed to = 6 m/s
wind direction = -225 degrees
Keep sending extra wind direction bytes to cause "J" error.
KNMI comments:
{
 # Gather a sample with multiple samples in a row having an error state.
}
Expect status code report of "J" for wind direction.
23:00   WR=UR="J", WS=US="D"
23:12   WR=UR="J"
23:24   WR=UR="J"
23:36   WR=UR="J"
23:48   WR=UR="J"
24:00   WR=UR="c"
24:00   WS=US="D"

(11)
23:56 to 26:56 of 60 minute interval
Purpose:
wind = 6 m/s
wind direction changed to 270 degrees
KNMI comments:
{
 # With another changed direction, see when the MD first starts being detected.
}
Expect detection of a MD event.  <MD#5> 225-270 degrees

(12)
26:56 to 27:56 of 60 minute interval
Purpose: too low wind speed
wind speed changed to = 4 m/s
wind direction = 270 degrees
KNMI comments:
{
 # Gather a sample but instead of having an error, have low wind speed.
}
Expect low wind speed.

(13)
27:56 to 30:56 of 60 minute interval
Purpose: MD
wind speed changed to = 6 m/s
wind direction changed to 315 degrees
KNMI comments:
{
 # With another changed direction, see when the MD first starts being detected.
}
Expect detection of a MD event.  270-315 degrees
28:00   WR=UR="c"
29:23.5 discontinuity_flag=True, ndiscontinuity_events = 8 <-- Start of <MD#8>.
29:35.5 discontinuity_flag=True, ndiscontinuity_events = 8
29:47.5 discontinuity_flag=True, ndiscontinuity_events = 8
29:59.5 discontinuity_flag=True, ndiscontinuity_events = 8
30:11.5 discontinuity_flag=True, ndiscontinuity_events = 8
30:23.5 discontinuity_flag=True, ndiscontinuity_events = 8
30:35.5 discontinuity_flag=True, ndiscontinuity_events = 8
30:47.5 discontinuity_flag=True, ndiscontinuity_events = 8


(14)
30:56 to 33:56 of 60 minute interval
Purpose: MD with speed change instead of direction
wind speed changed to = 40 m/s
wind direction = 315 degrees
KNMI comments:
{
# Add an MD where we change the wind speed instead of the direction.
# Gather 3 minutes of samples, to see when the MD first starts being detected.
}
Expect detection of a MD event.  6-40 m/s
31:11.5 discontinuity_flag=True, ndiscontinuity_events = 9 <-- Start of <MD#9>.
31:23.5 discontinuity_flag=True, ndiscontinuity_events = 9
31:35.5 discontinuity_flag=True, ndiscontinuity_events = 9


(X)
33:56 to 41.56 of 60 minute interval
Purpose: ramp wind speed and wind direction back to state (1) without causing errors or events.
(188700 - 152701) = 35999 , = 8 minutes
(40-6) m/s / (188700 - 152701)intervals = -0.001222256 m/s per 13.3333 mSec interval.
(315-45) degrees / (188700 - 152701)intervals = -0.007500208 degrees per 13.3333 mSec interval.
Start:
	wind speed = 40.0
wind direction = 315
-->
End:
wind speed = 6.0
wind direction = 45

(Y)
41:56 to 60:00 of 60 minute interval
Purpose: steady state that matches up to steady state conditoins of 00:00 to 10:00 of 60 minute interval
wind speed = 6.0
wind direction = 45