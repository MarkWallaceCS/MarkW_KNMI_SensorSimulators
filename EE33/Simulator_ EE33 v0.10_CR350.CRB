'CR350
'Simulator_ EE33 v0.9.CR6
'{



'\\ Description per markw@campbellsci.com, 435-227-9574, 03Oct2022.
'  Simulates polled EE33 temperature and RH sensor.  Returns AT, DP, and RH.
'  Every 250mSec scan checks for receipt of the polling message 0x&000067030003016E from a client device.
'  In the event of polling message received, the datalogger program responds with building and transmitting a EE33 data message.
'  The source data for the RH and air temperature in the response message comes from the TVF.CSV file (Test Value File) in the USR drive of the CR6.
'  The CR6 reads the TVF file every 250mSec.  The TVF file ontains 20 minutes of 4Hz records and is synchronized with the datalogger clock per 0 into 20 second interval.
'  You can edit the TVF.CSV with any test values you want.
'  Here's com watch on ComC3 on CR6 datalogger of receiving requests and responding with data message:
'{
'23:07:44.788 R 00 00 67 03 00 03 01 6E                          ..g....n
'23:07:45.005 T C3 37 67 0E 06 00 DF CF BA 41 EF DF 6D 41 66 66  .7g......A..mAff
'23:07:45.005 T 6B 42 13                                         kB.
'23:07:56.901 R 00 00 67 03 00 03 01 6E                          ..g....n
'23:07:57.004 T C3 37 67 0E 06 00 A2 C5 BB 41 A4 B1 71 41 EC 51  .7g......A..qA.Q
'23:07:57.004 T 6D 42 CB                                         mB.
'23:08:08.788 R 00 00 67 03 00 03 01 6E                          ..g....n
'23:08:09.004 T C3 37 67 0E 06 00 64 BB BC 41 91 80 75 41 71 3D  .7g...d..A..uAq=
'23:08:09.004 T 6F 42 B7                                         oB.
'23:08:20.788 R 00 00 67 03 00 03 01 6E                          ..g....n
'23:08:21.004 T C3 37 67 0E 06 00 27 B1 BD 41 C2 4C 79 41 F6 28  .7g...'..A.LyA.(
'23:08:21.004 T 71 42 E4                                         qB.
'}
'//



'\\ History per markw@campbellsci.com, 435-227-9574, 25Oct2022:
'v0.5 added
'//



'\\ Declarations:
'_<EE33>:
Const  ComPort_EE33 =Com2
Public TimeSinceRx_mSec As Long
Public DesiredDelay_mSec = 1500
Public ForceTxEE33SimMsgNow As Boolean
Public ForceTxEE33NackNow As Boolean, ErrorCode As Long =&hEC
Public nTimesToNotRespond As Long
Dim    TxMsgBytes As Long
Public EE33PollBytesRx
Dim    EE33PollStrRx As String *16
Dim    EE33PollByteRx(8) As UInt1
Dim    EE33PollByteLong As Long
Public EE33PollByteHex(8) As String *2
Dim    EE33SimByteLong As Long
Public EE33SimByteHex(19) As String
Dim    EE33SimByte(19) As UInt1
Dim    RspMsg As String *19
Dim    i33 As Long
'\<RTVF> Declarations for read in of Test Values File:
Dim    TopOfInterval(2) As Boolean
Alias  TopOfInterval(1)=TopOf20Min
Alias  TopOfInterval(2)=TopOf12SecWind
Public MainScanCntr As Long
Dim    AT, RH, DP, WD, WS
Const  UserDriveBytes=1E6 'Location of _TVF.CSV file (Test Values File).
Public RunTestValueFile As Boolean =True, SyncReadNow As Boolean =True
Public TVFStartedUp As Long
Public RTVFile As String *32 ="CPU:_TVF.CSV"
Public TVFHandle As Long
Dim    iRead As Long
Public StartupQSecInCSV As Long
Dim    nBytes
Dim    TestValueLine As String *250
Public TestValue(15) As String *16
Alias  TestValue( 1)=TV_RN
Alias  TestValue( 2)=TV_Minute
Alias  TestValue( 3)=TV_Second
Alias  TestValue( 4)=TV_Solar
Alias  TestValue( 5)=TV_BP
Alias  TestValue( 6)=TV_WSLower
Alias  TestValue( 7)=TV_WSUpper
Alias  TestValue( 8)=TV_WDUpper
Alias  TestValue( 9)=TV_WDLower
Alias  TestValue(10)=TV_LT1
Alias  TestValue(11)=TV_LT2
Alias  TestValue(12)=TV_UT1
Alias  TestValue(13)=TV_UT2
Alias  TestValue(14)=TV_RH
Alias  TestValue(15)=TV_Rain
'_
Public rTime(9)'Datalogger clock time.
Alias  rTime(1)=ClkYear
Alias  rTime(2)=ClkMonth
Alias  rTime(3)=ClkDay
Alias  rTime(4)=ClkHour
Alias  rTime(5)=ClkMinute
Alias  rTime(6)=ClkSecond
Alias  rTime(7)=ClkuSec
Alias  rTime(8)=ClkDayOfWeek
Alias  rTime(9)=ClkDayOfYear
'/<RTVF> declarations of for read in of Test Data Set.
'// Declarations.



'\\ Data Tables:

DataTable(_Main,True,-1)'Main readings used for source data.
  Sample(1,MainScanCntr,Long)
  Sample(1,WS,FP2)
  Sample(1,WD,FP2)
  Sample(1,AT,FP2)
  Sample(1,RH,FP2)
  Sample(1,DP,FP2)
EndTable


DataTable(_RxTVF,True,-1)'<RTVF>'Check data read from Test Value File.
  Sample( 1,nBytes,UINT2)
  Sample(15,TestValue(),IEEE4)
  Sample( 1,DP,IEEE4)
EndTable


DataTable(_RxTxEE33SimMsg,True,-1)
  Sample( 1,EE33PollBytesRx,UINT2)
  Sample( 8,EE33PollByteHex(),String)
  Sample(1,AT,IEEE4)
  Sample(1,DP,IEEE4)
  Sample(1,RH,IEEE4)
  Sample(19,EE33SimByteHex(),String)
EndTable


'// Data Tables.




'\\ Subroutines and Functions:

'\<RTVF> Run Test Data File: Test values for controlled testing of the processing for the SIAM messaging.
Sub sRunTestValueFile()
  If RunTestValueFile AND (TopOf20Min OR SyncReadNow) Then
    If TVFHandle>0 Then FileClose(TVFHandle)
    TVFHandle=FileOpen(RTVFile,"r",0)
    If TVFHandle>0 Then
      FileReadLine(TVFHandle,TestValueLine,250)'Read over 1st header line (column#).
      FileReadLine(TVFHandle,TestValueLine,250)'Read over 2nd header line (field name).
      StartupQSecInCSV= (ClkMinute*60*4+ClkSecond*4+(4*ClkuSec/1e6)) MOD 4800
      If SyncReadNow Then
        For iRead= 1 To StartupQSecInCSV-1
          FileReadLine(TVFHandle,TestValueLine,250)
        Next iRead
        SyncReadNow=False
      EndIf
    Else
      RunTestValueFile=False
    EndIf'TVFHandle>0
  EndIf'If RunTestValueFile And (IfTime(0,15,Min) Or SyncReadNow) Then
  If TVFHandle>0 Then
    nBytes=FileReadLine(TVFHandle,TestValueLine,175)
    If nBytes>50 Then SplitStr(TestValue(),TestValueLine,"",15,0)
  EndIf
  If NOT RunTestValueFile AND TVFHandle>0 Then FileClose(TVFHandle)
EndSub'/ RunTestValueFile().
'/



'\<EE33><AT><DP><RH>
Sub BuildEE33SimMsg()

  Erase(EE33SimByte)

  If ForceTxEE33NackNow Then

    EE33SimByte( 1)=&hC3
    EE33SimByte( 2)=&h37
    EE33SimByte( 3)=&h67'Answer to poll
    EE33SimByte( 4)=&h0E'Number of data bytes.
    EE33SimByte( 5)=&h15'0x15=NAck
    EE33SimByte( 6)=ErrorCode'ErrorCodeByte
    'ErrorCode As Long =&hEC 'No calibration data
    TxMsgBytes=6

  Else

    EE33SimByte( 1)=&hC3
    EE33SimByte( 2)=&h37
    EE33SimByte( 3)=&h67'Answer to poll
    EE33SimByte( 4)=&h0E'Number of data bytes.
    EE33SimByte( 5)=&h06'6=Ack
    EE33SimByte( 6)=&h00'Units indicator byte.  0 indicates metric units
    'EE33SimByte( 7-10) 'Temperature in DgC
    'EE33SimByte(11-14) 'DewPoint n DgC
    'EE33SimByte(15-18) 'RH in percent
    'EE33SimByte(19)    'Checksum modulo 0x100

    DewPoint(DP,AT,RH)

    '_Read in test values from test values file.  This is from .CSV in the user drive of the datalogger.
    MoveBytes(EE33SimByte(), 6,AT,0,4,2)
    MoveBytes(EE33SimByte(),10,DP,0,4,2)
    MoveBytes(EE33SimByte(),14,RH,0,4,2)

    TxMsgBytes=18

  EndIf'ForceAnE33ErrorMsgNow


  Erase(RspMsg)
  For i33= 1 To TxMsgBytes
    MoveBytes(RspMsg,i33-1,EE33SimByte(i33),0,1)
    EE33SimByteLong=EE33SimByte(i33)
    EE33SimByteHex(i33)=FormatLong(EE33SimByteLong,"%02X")
  Next i33
  EE33SimByte(TxMsgBytes+1)=CheckSum(RspMsg,7,TxMsgBytes)'Checksum modulo 0x100
  MoveBytes(RspMsg,TxMsgBytes,EE33SimByte,TxMsgBytes,1)
  EE33SimByteLong=EE33SimByte(TxMsgBytes+1)
  EE33SimByteHex(TxMsgBytes+1)=FormatLong(EE33SimByteLong,"%02X")

EndSub'BuildEE33SimMsg()
'/


'// Subroutines and Functions.




BeginProg

  '\ Initializations:
  SerialOpen(ComPort_EE33, 9600, 3, 0, 500, 4) ' CR6_ComC3, RS485 half duplex 8N1.
  '/


  Scan(250, mSec, 3, 0)



    '\Get source data from data values file:
    RealTime(rTime())'Datalogger clock time.

    If IfTime(0,20,Min) Then TopOf20Min=True
    If RunTestValueFile Then
      Call sRunTestValueFile() : TopOf20Min=False
      RH=TV_RH
      AT=TV_LT1
      WD=TV_WDUpper
      WS=TV_WSUpper
      If TVFStartedUp=0 Then TVFStartedUp=1
    Else
      If TVFHandle>0 Then FileClose(TVFHandle)'Need to close file in file system if uploading replacement test file.
    EndIf'/
    If TVFStartedUp > 0 Then TVFStartedUp += 1
    If TVFStartedUp=10 Then'Clear-out skipped scans and other caused by synchronizing TDF file read.
      SetStatus("SkippedScan",0)
      ' SetStatus("MaxBuffDepth",0)
      SetStatus("MaxProcTime",0)
      SetStatus("MaxSlowProcTime(1)",0)
    EndIf

    RH=TV_RH
    AT=TV_LT1
    WD=TV_WDUpper
    WS=TV_WSUpper


    CallTable _Main
    CallTable _RxTVF
    '/


    '\<EE33><AT><DP><RH> Check for request for EE33 data.  If request received respond with building and transmitting simulated message:

    EE33PollBytesRx=SerialInChk(ComPort_EE33)

    If TimeSinceRx_mSec = 0 AND EE33PollBytesRx >= 8 Then

      SerialInBlock(ComPort_EE33,EE33PollStrRx,8)
      For i33= 1 To 8
        MoveBytes(EE33PollByteRx,i33-1,EE33PollStrRx,i33-1,1)
        EE33PollByteLong=EE33PollByteRx(i33)
        EE33PollByteHex(i33)=FormatLong(EE33PollByteLong,"%02X")
      Next i33

      Timer(1,mSec,2)'Timer#1: Reset and start.

    EndIf


    TimeSinceRx_mSec=Timer(1,mSec,4)'Timer#1: Read.
    If TimeSinceRx_mSec + 250 > DesiredDelay_mSec OR ForceTxEE33SimMsgNow Then

      If (EE33PollByteRx(3) = &h67 AND EE33PollByteRx(4) = &h03) OR ForceTxEE33SimMsgNow Then
        Call BuildEE33SimMsg()
        If nTimesToNotRespond < 1 Then SerialOutBlock(ComPort_EE33, RspMsg, TxMsgBytes+1)
        nTimesToNotRespond -= 1
        CallTable _RxTxEE33SimMsg
      EndIf'EE33PollStrRx="00RDD}"

      Timer(1,mSec,3)'Timer#1: Stop and reset.
      ForceTxEE33SimMsgNow=False
      SerialFlush(ComPort_EE33)


    EndIf'SerialInChk(ComPort_EE33)>=9


    '/





  NextScan




EndProg
'}
