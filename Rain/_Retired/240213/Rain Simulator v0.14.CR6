'Rain Simulator v0.14.CR6
'v0.13, 240131, markw@campbellsci.com
'v0.14, 240206, markw@campbellsci.com
'{



'\
'ControlPort_C1 listens for flush trigger from KNMI CR1000X.
'ControlPort_C2 is empty valve status line.  Normally Hi (5V).  Set Lo (0V) while emptying.
'ControlPort_C4 is frequency output signal proportional to float level.
'/



'\\ Declarations Section:
Const  FLOAT_MULT_mm_PER_HZ = 0.001 ' (11.0 - 1.0)mm / (11000 - 1000)Hz
Const  FLOAT_OSET_mm = 0.0
Public EmptyRate_mmPerSec =  1.2
Public FillRate_mmPerHr   = 20.0
Public Float_mm = 1.01
Public Rain_Hz
Public PWM_DutyCycle_Frac = 0.5
Public PWM_Period_uSec
Public Empty_Valve_Closed As Boolean = True, Empty_Valve_5V  ' When valve is closed port state is Hi/True/5vdc.
Dim    TrigPulseDetect As Boolean
Public DontDrop5VOnEmpty As Boolean
Public NotClogged As Boolean = True
Public ForceEmptyNow As Boolean
Public ForceZeroOutput As Boolean
Public nTrigPulsesRx As Long
Public ValveOpenTimer_mSec
Public ApplyDecayingSinusoid As Boolean
Public MinimumLengthOfBouncing = 32 ' 250mSec per count.
Public MaximumLengthOfBouncing = 64 ' 250mSec per count.
Public SinusoidCnts As Long
Public Bounce_mm
Public ExtraAmplitudeStartValue_mm  = 0.08
Public ExtraAmplitude_mm = 0.08
Public AddSomeExtra_mm
Public AmplitudeDecrement = 0.002
'// End of declarations section.



'\\ Data tables section:

DataTable(EmptyTriggerEvents, ValveOpenTimer_mSec > 0, -1)
  Sample(1, ValveOpenTimer_mSec, IEEE4)
  Sample(1, Float_mm           , IEEE4)
EndTable

DataTable(FloatLevel, True, -1)
  Sample(1, Float_mm             , IEEE4)
  Sample(1, ApplyDecayingSinusoid, FP2)
  Sample(1, SinusoidCnts         , Long)
  Sample(1, Bounce_mm            , IEEE4)
EndTable

'//  End of data tables section.



'\\ Rain Simulator datalogger program.
BeginProg


  Scan(250, mSec, 10, 0)


    If ApplyDecayingSinusoid Then
      SinusoidCnts += 1
      If (SinusoidCnts > MinimumLengthOfBouncing AND ExtraAmplitude_mm < 0.0005) OR SinusoidCnts > MaximumLengthOfBouncing Then
        SinusoidCnts = 0
        ApplyDecayingSinusoid = False
        ExtraAmplitude_mm = ExtraAmplitudeStartValue_mm
        Bounce_mm = 0.0
      Else
        ExtraAmplitude_mm -= AmplitudeDecrement
        Bounce_mm = ExtraAmplitude_mm * SIN(SinusoidCnts - 8  * 2 * 3.141593 / 16) ' Input angle is in radians.  + 4 is for 90Dg phase shift on start.
      EndIf
    ElseIf FillRate_mmPerHr > 0 Then
      Bounce_mm = 0
    EndIf

    Float_mm += FillRate_mmPerHr/4/3600 + Bounce_mm + AddSomeExtra_mm
    If AddSomeExtra_mm > 0 AND Float_mm > 1.1 Then AddSomeExtra_mm = 0.0

    If Float_mm < 0.92 AND ABS(Bounce_mm) > 0.15 Then
      ApplyDecayingSinusoid = False
    EndIf

    '\ Rain gauge frequency signal output:
    Rain_Hz = (Float_mm - FLOAT_OSET_mm) / FLOAT_MULT_mm_PER_HZ
    PWM_Period_uSec = INT( 1e6 / Rain_Hz )
    If ForceZeroOutput Then
      PWM_DutyCycle_Frac = 0.0
    Else
      PWM_DutyCycle_Frac = 0.5
    EndIf
    PWM(PWM_DutyCycle_Frac, C4, PWM_Period_uSec, uSec)
    '/

    '\ Respond to trigger by triggering empty of rain gauge:
    If TrigPulseDetect OR ForceEmptyNow Then
      If ForceEmptyNow Then
        ForceEmptyNow = False
      Else
        nTrigPulsesRx += 1
      EndIf
      Empty_Valve_Closed = False
      ValveOpenTimer_mSec = Timer(1, mSec, 0) ' Timer#1(Start)
      TrigPulseDetect = False
    EndIf
    '/

    ValveOpenTimer_mSec = Timer(1, mSec, 4) ' Timer#1(Read)
    If ValveOpenTimer_mSec > 0 AND NotClogged Then
      Float_mm -= EmptyRate_mmPerSec/4
      If Float_mm < 1.1 Then
        Empty_Valve_Closed = True
        ValveOpenTimer_mSec = Timer(1, mSec,3) ' Timer#1(Stop&Reset)
        ApplyDecayingSinusoid = True
        If FillRate_mmPerHr < 1E-6 Then AddSomeExtra_mm = 0.05
      EndIf
    EndIf

    '\ Set Empty_Valve_Closed state:
    If Empty_Valve_Closed Then
      Empty_Valve_5V = 1
    Else
      If DontDrop5VOnEmpty Then
        Empty_Valve_5V = 1
      Else
        Empty_Valve_5V = 0
      EndIf
    EndIf
    PortSet(C2, Empty_Valve_5V, 0)
    '/

    CallTable(EmptyTriggerEvents)
    CallTable(FloatLevel)


  NextScan



  SlowSequence'#1
  Do
    Do
      WaitDigTrig(C1, 1) ' Edge trigged Hi to Lo transition.
      If NOT DontDrop5VOnEmpty Then PortSet(C2, 0, 0)
      Empty_Valve_5V = 0
      TrigPulseDetect = True
      ExitDo
    Loop
    Scan(100, mSec, 0, 0)
      If NOT TrigPulseDetect Then
        ExitScan
      EndIf
    NextScan
  Loop
  EndSequence'#1



EndProg
'// Rain Simulator datalogger program.
'}
